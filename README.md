Commerce Paysera
=======================

Version: 1.0.3

Date: 2021-09-21

Contributors: Paysera

Tags: online payment, payment, payment gateway, SMS payments, international payments, mobile payment, sms pay, payment 
by sms, billing system, payment institution, macro payments, micro payments, sms bank

Requires at least: 9.0

Tested up to: 9.2

Stable tag: 1.0.3

Requires PHP: 7.4

Minimum requirements: commerce, commerce_payment, commerce_order, commerce_checkout, commerce_price, commerce_store, commerce_log

License: GPLv3

License URL: http://www.gnu.org/licenses/gpl-3.0.html

Description
-----------

Collecting payments with Paysera is simple, fast and secure. It is enough to open Paysera account, install the plug-in 
to your store and you will be able to use all of the most popular ways of payment collection in one place.
No need for complicated programming or integration work. With Paysera you will receive real time notifications 
about successful transactions in your online store, and the money will reach you sooner. No monthly, registration 
or connection fee.

It is simple to use and administer payment collection, and you can monitor movement of funds in your smart phone.
Client services and consultation takes place 7 days a week, from 8:00 till 20:00 (EET)
Payments are made in real time.

Paysera applies the lowest fees on the market, and payments from foreign banks and systems are converted 
at best possible rates.

To use Paysera payment gateway, register at paysera.com and create your project. To apply for Paysera go to this link: 
https://bank.paysera.com/en/registration#/registration. Our extension, your account and support is free. 
Only for selling a transaction fee is charged. For further information regarding the Paysera fees please visit: 
https://www.paysera.com/v2/en-GB/fees. 

== Benefits and possibilities of using Paysera checkout ==

 - Plenty of payment methods. Payments via more than 10 000 local and foreign banks, and with Visa, MasterCard, 
 Maestro cards. Payments via international payments systems, in cash and directly via Paysera.
 - Plenty of countries and currencies. Service your clients from all over the world. Paysera operates in 184 
 countries and supports more than 30 currencies. The payment window is translated to 18 languages. Let your clients 
 pay you conveniently and provide information the language they can understand.
 - Convenient administration. It is easy to manage one or several e-payment projects, as the account can be accessed 
 at any time of the day and from anywhere by logging in to Paysera e-payments system. 
 - Perhaps the lowest price on the market. Save money with Paysera Checkout. Our prices are among the best on the 
 market and we apply very favorable currency exchanges rates when executing transfers from foreign banks and systems.
 - Real-time notifications. Receive information about a successful payment immediately and issue purchases to 
 clients faster. Be informed about a payment error instantly and return money with a click of a button.
 - All income to one account. It is easy to monitor and manage income when you do not have many accounts in 
 different banks. Income is collected to one Paysera account, which makes it even easier to see 
 the detailed statistics and form reports.
 - Safe payments online. Place the special sign "Safe payment online" and let your clients know that it is safe 
 to purchase goods or services on your website, as you are using Paysera solution for safe payments.
 - Leasing services. Provide your clients with the possibility of hire purchase. Along with payment methods, offer 
 leasing services provided by General Financing and MokiLizingas.
 
 Client services and consultation takes place 7 days a week, from 8:00 till 20:00 (EET).


Installation
------------

-= Installation by FTP =-

1. Download Paysera plugin zip.

2. Connect to server and go to Drupal base directory

3. Go to folder /modules

4. Extract the content of Paysera plugin zip file
   
5. Require dependencies with composer. Run this command in your Drupal directory:
    composer require "webtopay/libwebtopay":"^1.6"
    
6. Go to the:
   Extend -> COMMERCE

7. Check Drupal Commerce Paysera Gateway and press Install

8. Configure Drupal Commerce Paysera gateway:
   Commerce -> Configuration -> Payment gateways -> Add new payment gateway
   
   Machine-readable name: paysera_commerce_payment_gateway
   Plugin: Paysera
   
   Note: you must enter project id and sign from Paysera project settings to be able use it.
   
9. Save changes.

-= Installation from admin panel =-

1. Download Paysera plugin zip.

2. Require webtopay/libwebtopay library with composer. Run this command in your Drupal directory:
    composer require "webtopay/libwebtopay":"^1.6"

3. Connect to Drupal admin panel.

4. Install Paysera plugin to Drupal:
    Extend -> + Install new module -> Choose file your downloaded plugin zip -> Press install

5. Go to the:
   Extend -> COMMERCE
   
6. Find Drupal Commerce Paysera Gateway and press Install

7. Configure Drupal Commerce Paysera gateway:
   Commerce -> Configuration -> Payment gateways -> Add new payment gateway
   
   Machine-readable name: paysera_commerce_payment_gateway
   Plugin: Paysera
   
   Note: you must enter project id and sign from Paysera project settings to be able use it.
   
8. Save changes.

Changelog
---------
= 1.0.3 =
* Update - drupal version

= 1.0.2 =
* Update - translations

= 1.0.1 =
* Update - translations
* Update - new payment method parameter
* Update - checkout display changes

= 1.0.0 =
* First release

Upgrade Notice
--------------
= 1.0.3 =

Raised supporting version

= 1.0.2 =

Updated plugin translations, changed title

= 1.0.1 =

Updated plugin translations, code updates.

= 1.0.0 =

First release

Support
-------

For any questions, please look for the answers at https://support.paysera.com or contact customer support center 
by email  support@paysera.com or by phone +44 20 80996963 | +370 700 17217.

For technical documentation visit: https://developers.paysera.com/
