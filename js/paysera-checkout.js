(function ($) {
    $('#paysera_payment_country').change(function () {
        $('.paysera-methods').hide('slow');

        $('#paysera_payment_country_' + $(this).val()).show('slow');
    });
    $('input[name="paysera_payment_radio"]').change(function () {
        $('#paysera_payment_method').val($(this).val());
    });
})(jQuery);
