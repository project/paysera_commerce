<?php
namespace Drupal\commerce_paysera\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_paysera\Plugin\Commerce\PaymentGateway\PayseraPaymentGateway;
use Drupal\commerce_paysera\Service\PayseraSettingsHelper;
use Drupal\Core\TypedData\Exception\MissingDataException;
use \Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_paysera\Service\PayseraHelper;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Core\Form\FormStateInterface;
use WebToPayException;
use WebToPay_Factory;
use WebToPay;
use Drupal;

class PayseraOffsiteForm extends BasePaymentOffsiteForm
{
    /**
     * @var PayseraHelper
     */
    private $payseraHelper;
    private $pluginSettings;

    public function __construct()
    {
        $this->payseraHelper = Drupal::service('commerce_paysera.paysera_helper');
        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     * @throws NeedsRedirectException
     */
    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildConfigurationForm($form, $form_state);

        /** @var Payment $payment */
        $payment = $this->getEntity();

        $order = $payment->getOrder();

        /** @var PayseraPaymentGateway $payment_gateway_plugin */
        $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

        $requestParameters = [];

        try {
            /** @var AddressItem $billingAddress */
            $billingAddress = $order->getBillingProfile()->get('address')->first();

            $requestParameters['p_firstname'] = $billingAddress->getGivenName();
            $requestParameters['p_lastname'] = $billingAddress->getFamilyName();
            $requestParameters['p_street'] = $billingAddress->getAddressLine1();
            $requestParameters['p_countrycode'] = $billingAddress->getCountryCode();
            $requestParameters['p_zip'] = $billingAddress->getPostalCode();
        } catch (MissingDataException $exception) {
            $this->payseraHelper->getPayseraLogger()->warning(
                'Form builder, address exception: ' . $exception->getMessage(),
                ['exception' => $exception,]
            );
        }

        $paymentCountry = $payment->getOrder()->getData('paysera_payment_country');
        $paymentMethod = $payment->getOrder()->getData('paysera_payment_method');

        try {
            $requestParameters['amount'] = $this->payseraHelper->convertOrderAmountToString(
                $order->getTotalPrice()->getNumber()
            );
            $requestParameters['lang'] = $this->payseraHelper->drupalLanguageToPaysera(
                $this->payseraHelper->getCurrentLanguage()
            );
            $requestParameters['projectid'] = $this->pluginSettings->getProjectId();
            $requestParameters['sign_password'] = $this->pluginSettings->getProjectSign();
            $requestParameters['orderid'] = $order->id();
            $requestParameters['currency'] = $payment->getAmount()->getCurrencyCode();
            $requestParameters['p_email'] = $order->getEmail();
            $requestParameters['country'] = $paymentCountry;
            $requestParameters['payment'] = $paymentMethod;
            $requestParameters['accepturl'] = $this->payseraHelper->getBaseUrl() . 'paysera_accept/';
            $requestParameters['cancelurl'] = $form['#cancel_url'];
            $requestParameters['callbackurl'] = $payment_gateway_plugin->getNotifyUrl()->toString();
            $requestParameters['test'] = $this->payseraHelper->commerceModeToPaysera($payment_gateway_plugin->getMode());
            $requestParameters['buyer_consent'] = (int) $this->pluginSettings->isBuyerConsentEnabled();

            $paymentStatus = $this->pluginSettings->getPaymentStatusPending();

            if (!$this->payseraHelper->isPaymentCreated($order->id(), $paymentStatus)) {
                $this->payseraHelper->createPayment($order->id(), $requestParameters, $paymentStatus);
            }

            $this->payseraHelper->setOrderLog($order, t('Payment checkout process is started'));

            $redirectUrl = (new WebToPay_Factory(
                [
                    'projectId' => $this->pluginSettings->getProjectId(),
                    'password' => $this->pluginSettings->getProjectSign(),
                ]
            ))
                ->getRequestBuilder()
                ->buildRequestUrlFromData($requestParameters)
            ;
        } catch (WebToPayException $exception) {
            $this->payseraHelper->getPayseraLogger()->warning(
                'Form builder, webtopay exception',
                ['exception' => $exception,]
            );

            Drupal::messenger()->addWarning(t('Error occurred with payment gateway module'));

            $redirectUrl = $this->payseraHelper->getBaseUrl();
        }

        return BasePaymentOffsiteForm::buildRedirectForm($form, $form_state, $redirectUrl, [], self::REDIRECT_GET);
    }
}
