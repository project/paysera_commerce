<?php
namespace Drupal\commerce_paysera\Normalizer;

use Drupal\commerce_paysera\Entity\PluginSettings;
use Drupal\Core\Config\Config;

class PluginSettingsNormalizer
{
    /**
     * @param Config $config
     * @return PluginSettings
     */
    public function normalizeProjectSettings(Config $config)
    {
        $projectSettings = (new PluginSettings())
            ->setProjectId((int) $config->get('project_id'))
            ->setProjectSign($config->get('project_sign'))
            ->setCheckoutDescription($config->get('checkout_description'))
            ->setPaymentListEnabled((bool) $config->get('payment_list_mode'))
            ->setEnabledCountries($config->get('enabled_countries'))
            ->setGridEnabled((bool) $config->get('grid_mode'))
            ->setBuyerConsentEnabled((bool) $config->get('buyer_consent'))
            ->setPaymentStatusNew($config->get('payment_status_new'))
            ->setPaymentStatusConfirmed($config->get('payment_status_confirmed'))
            ->setPaymentStatusPending($config->get('payment_status_pending'))
            ->setQualitySignEnabled((bool) $config->get('quality_sign_mode'))
            ->setOwnershipCodeEnabled((bool) $config->get('ownership_code_mode'))
            ->setOwnershipCode($config->get('ownership_code'))
        ;

        return $projectSettings;
    }
}
