<?php
namespace Drupal\commerce_paysera\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Annotation\CommercePaymentGateway;
use Drupal\commerce_paysera\Service\PayseraSettingsHelper;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_paysera\Service\PayseraHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentTypeManager;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Annotation\Translation;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_price\Price;
use UnexpectedValueException;
use Exception;
use WebToPay;
use Drupal;

/**
 * @CommercePaymentGateway(
 *   id = "paysera_commerce_payment_gateway",
 *   label = @Translation("Paysera"),
 *   display_label = @Translation("All popular payment methods"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_paysera\PluginForm\OffsiteRedirect\PayseraOffsiteForm",
 *   }
 * )
 */
class PayseraPaymentGateway extends OffsitePaymentGatewayBase
{
    private $pluginSettings;

    /**
     * @var PayseraHelper
     */
    private $payseraHelper;

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityTypeManagerInterface $entity_type_manager,
        PaymentTypeManager $payment_type_manager,
        PaymentMethodTypeManager $payment_method_type_manager,
        TimeInterface $time
    ) {
        parent::__construct(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $entity_type_manager,
            $payment_type_manager,
            $payment_method_type_manager,
            $time
        );

        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
        $this->payseraHelper = Drupal::service('commerce_paysera.paysera_helper');
    }

    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        $form['main_settings'] = [];

        $form['extra_settings'] = [
            '#type' => 'details',
            '#title' => t('Extra Settings'),
        ];

        $form['order_statuses'] = [
            '#type' => 'details',
            '#title' => t('Payment Status'),
        ];

        $form['project_additional'] = [
            '#type' => 'details',
            '#title' => t('Project additions'),
        ];

        $form['main_settings']['project_id'] = [
            '#type' => 'textfield',
            '#default_value' => $this->pluginSettings->getProjectId(),
            '#title' => t('Project ID'),
            '#description' => t('Project id'),
            '#required' => true,
        ];

        $form['main_settings']['project_sign'] = [
            '#type' => 'textfield',
            '#default_value' => $this->pluginSettings->getProjectSign(),
            '#title' => t('Sign'),
            '#description' => t('Paysera project sign password'),
            '#required' => true,
        ];

        $form['extra_settings']['checkout_description'] = [
            '#type' => 'textarea',
            '#default_value' => t($this->pluginSettings->getCheckoutDescription()),
            '#title' => t('Description'),
            '#cols' => 100,
            '#rows' => 3,
            '#description' => t('This controls the description which the user sees during checkout.'),
            '#attributes' => [
                'size' => '60',
                'style' => 'width:50%;'
            ],
        ];

        $form['extra_settings']['payment_list_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('List of payments'),
            '#description' => t('Enable this to display payment methods list at checkout page'),
            '#default_value' => $this->pluginSettings->isPaymentListEnabled(),
        ];

        $form['extra_settings']['enabled_countries'] = [
            '#type' => 'select',
            '#title' => t('Specific countries'),
            '#options' => $this->payseraHelper->getProjectCountries($this->pluginSettings->getProjectId()),
            '#description' => t('Select which country payment methods to display (empty means all)'),
            '#size' => 5,
            '#default_value' => $this->pluginSettings->getEnabledCountries(),
            '#multiple' => true,
            '#attributes' => [
                'style' => 'width:50%;'
            ],
        ];

        $form['extra_settings']['grid_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Grid view'),
            '#description' => t('Enable this to use payment methods grid view'),
            '#default_value' => $this->pluginSettings->isGridEnabled(),
        ];

        $form['extra_settings']['buyer_consent'] = [
            '#type' => 'checkbox',
            '#title' => t('Buyer consent'),
            '#description' => t('Enable this to skip the additional step when using PIS payment methods'),
            '#default_value' => $this->pluginSettings->isBuyerConsentEnabled(),
        ];

        $form['order_statuses']['payment_status_new'] = [
            '#type' => 'select',
            '#title' => t('New Payment Status'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusNew(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;'
            ],
        ];

        $form['order_statuses']['payment_status_confirmed'] = [
            '#type' => 'select',
            '#title' => t('Paid Payment Status'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusConfirmed(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;'
            ],
        ];

        $form['order_statuses']['payment_status_pending'] = [
            '#type' => 'select',
            '#title' => t('Pending checkout'),
            '#options' => $this->payseraHelper->getAvailablePaymentStatuses(),
            '#default_value' => $this->pluginSettings->getPaymentStatusPending(),
            '#attributes' => [
                'style' => 'width:50%;height:28px;'
            ],
        ];

        $form['project_additional']['quality_sign_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Quality sign'),
            '#description' => t('Enable this to use quality sign'),
            '#default_value' => $this->pluginSettings->isQualitySignEnabled(),
        ];

        $form['project_additional']['ownership_code_mode'] = [
            '#type' => 'checkbox',
            '#title' => t('Ownership of website'),
            '#description' => t('Enable this to place ownership code'),
            '#default_value' => $this->pluginSettings->isOwnershipCodeEnabled(),
        ];

        $form['project_additional']['ownership_code'] = [
            '#type' => 'textfield',
            '#title' => t('Ownership code'),
            '#default_value' => $this->pluginSettings->getOwnershipCode(),
        ];

        return parent::buildConfigurationForm($form, $form_state);
    }

    public function validateConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValue($form['#parents']);

        if (filter_var($values['main_settings']['project_id'], FILTER_VALIDATE_INT) === false) {
            $form_state->setErrorByName(
                'project_id',
                ($this->t('Project ID') . ' ' . $this->t('not valid'))
            );
        }

        if (
            (bool) $values['project_additional']['ownership_code_mode']
            && empty($values['project_additional']['ownership_code'])
        ) {
            $form_state->setErrorByName(
                'ownership_code',
                ($this->t('Ownership code')  . ' ' .  $this->t('not valid'))
            );
        }

        return parent::validateConfigurationForm($form, $form_state);
    }

    public function submitConfigurationForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValue($form['#parents']);

        PayseraSettingsHelper::getPayseraConfig()
            ->set('project_id', $values['main_settings']['project_id'])
            ->set('project_sign', $values['main_settings']['project_sign'])
            ->set('checkout_description', $values['extra_settings']['checkout_description'])
            ->set('payment_list_mode', $values['extra_settings']['payment_list_mode'])
            ->set('enabled_countries', $values['extra_settings']['enabled_countries'])
            ->set('grid_mode', $values['extra_settings']['grid_mode'])
            ->set('buyer_consent', $values['extra_settings']['buyer_consent'])
            ->set('payment_status_new', $values['order_statuses']['payment_status_new'])
            ->set('payment_status_confirmed', $values['order_statuses']['payment_status_confirmed'])
            ->set('payment_status_pending', $values['order_statuses']['payment_status_pending'])
            ->set('quality_sign_mode', $values['project_additional']['quality_sign_mode'])
            ->set('ownership_code_mode', $values['project_additional']['ownership_code_mode'])
            ->set('ownership_code', $values['project_additional']['ownership_code'])
            ->save()
        ;

        parent::submitConfigurationForm($form, $form_state);
    }

    public function onNotify(Request $request)
    {
        parent::onNotify($request);

        try {
            $response = WebToPay::validateAndParseData(
                $this->payseraHelper->getRequestArray($request),
                $this->pluginSettings->getProjectId(),
                $this->pluginSettings->getProjectSign()
            );

            if ($response['type'] !== 'macro') {
                $this->payseraHelper->getPayseraLogger()->warning(
                    'onNotify endpoint got an callback with type not equal to macro',
                    [$response,]
                );

                throw new UnexpectedValueException('Only macro payment callbacks are accepted');
            }

            $orderId = $response['orderid'];

            /** @var OrderInterface $order */
            $order = Order::load($orderId);

            if ($order !== null) {
                /** @var Price $totalPrice */
                $totalPrice = $order->getTotalPrice();

                $orderMoney = [
                    'amount' => $this->payseraHelper->convertOrderAmountToString($totalPrice->getNumber()),
                    'currency' => $totalPrice->getCurrencyCode(),
                ];

                $paymentErrorMessage = $this->checkForPaymentErrorMessage($orderMoney, $response);

                if ($paymentErrorMessage === null) {
                    $paymentState = $this->pluginSettings->getPaymentStatusConfirmed();

                    if (
                        $response['status'] === '1'
                        && !$this->payseraHelper->isPaymentCreated($orderId, $paymentState)
                    ) {
                        $this->payseraHelper->createPayment(
                            $orderId,
                            $response,
                            $paymentState
                        );

                        $this->payseraHelper->setOrderLog($order, t('Callback payment completed'));

                        echo 'OK, payment was initiated';
                    }
                } else {
                    echo $paymentErrorMessage;
                }
            } else {
                echo 'ERROR, order doesnt exists';
            }
        } catch (Exception $exception) {
            echo 'EXCEPTION, ' . $exception->getMessage();

            $this->payseraHelper->getPayseraLogger()->warning('Callback exception: ' . $exception->getMessage());
        }
    }

    public function onCancel(OrderInterface $order, Request $request)
    {
        parent::onCancel($order, $request);

        $this->payseraHelper->setOrderLog($order, t('User returned to cancelurl'));
    }

    /**
     * @param array $orderMoney
     * @param array $response
     * @return string|null
     */
    private function checkForPaymentErrorMessage(array $orderMoney, array $response)
    {
        $orderAmount = $orderMoney['amount'];
        $orderCurrency = $orderMoney['currency'];

        if ($response['amount'] !== $orderAmount || $response['currency'] !== $orderCurrency) {
            $checkConvert = array_key_exists('payamount', $response);

            if (!$checkConvert) {
                return (
                    'Wrong amount: ' . $response['amount'] / 100 . $response['currency']
                    . ', expected: ' . $orderAmount / 100 . $orderCurrency
                );
            } elseif ($response['payamount'] !== $orderAmount || $response['paycurrency'] !== $orderCurrency) {
                return (
                    'Wrong pay amount: ' . $response['payamount'] / 100 . $response['paycurrency']
                    . ', expected: ' . $orderAmount / 100 . $orderCurrency
                );
            }
        }

        return null;
    }
}
