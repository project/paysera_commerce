<?php
namespace Drupal\commerce_paysera\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Annotation\CommerceCheckoutPane;
use Drupal\commerce_paysera\Service\PayseraSettingsHelper;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce_paysera\Service\PayseraHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Annotation\Translation;
use WebToPay_PaymentMethodList;
use WebToPay_PaymentMethod;
use WebToPayException;
use WebToPay;
use Drupal;

/**
 * @CommerceCheckoutPane(
 *  id = "paysera_order_fields",
 *  label = @Translation("Paysera paysera order fields"),
 *  admin_label = @Translation("Paysera paysera order fields"),
 *  display_label = @Translation("All popular payment methods"),
 *  default_step = "review",
 * )
 */
class PayseraOrderFields extends CheckoutPaneBase implements CheckoutPaneInterface
{
    const GRID_VIEW_ROWS_NUMBER = 3;

    /**
     * @var PayseraHelper
     */
    private $payseraHelper;
    private $pluginSettings;

    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        CheckoutFlowInterface $checkout_flow,
        EntityTypeManagerInterface $entity_type_manager
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

        $this->payseraHelper = Drupal::service('commerce_paysera.paysera_helper');
        $this->pluginSettings = PayseraSettingsHelper::getPluginSettings();
    }

    public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form)
    {
        try {
            /** @var $billingAddress AddressItem */
            $billingAddress = $this->order->getBillingProfile()->get('address')->first();

            $billingCountry = strtolower($billingAddress->getCountryCode());
            $paymentGateway = $this->order->get('payment_gateway')->first()->getValue()['target_id'];
        } catch (MissingDataException $exception) {
            $paymentGateway = 'default';
            $billingCountry = 'unknown';

            $this->payseraHelper->getPayseraLogger()->warning(
                'buildPaneForm data exception: ' . $exception->getMessage()
            );
        }

        $pane_form['#attached']['library'][] = 'commerce_paysera/paysera-checkout';

        if (!empty($this->pluginSettings->getCheckoutDescription())) {
            $pane_form['paysera_checkout']['checkout_description'] = [
                '#markup' => $this->pluginSettings->getCheckoutDescription(),
            ];
        }

        if ($paymentGateway === PayseraHelper::PAYSERA_COMMERCE_MACHINE_PLUGIN_NAME) {
            if ($this->pluginSettings->isPaymentListEnabled()) {
                try {
                    $amount = $this->payseraHelper->convertOrderAmountToString(
                        $this->order->getTotalPrice()->getNumber()
                    );
                    $currency = $this->order->getTotalPrice()->getCurrencyCode();

                    $paymentMethodsInfo = WebToPay::getPaymentMethodList(
                        $this->pluginSettings->getProjectId(),
                        $this->order->getTotalPrice()->getCurrencyCode()
                    )
                        ->setDefaultLanguage($this->payseraHelper->getCurrentLanguage())
                        ->filterForAmount($amount, $currency)
                    ;
                } catch (WebToPayException $exception) {
                    $this->payseraHelper->getPayseraLogger()->warning(
                        'buildPaneForm payment list exception: ' . $exception->getMessage()
                    );

                    return $pane_form;
                }

                $activeCountry = function () use ($paymentMethodsInfo, $billingCountry) {
                    $enabledCountries = $this->pluginSettings->getEnabledCountries();

                    if (!empty($this->pluginSettings->getEnabledCountries())) {
                        if (!in_array($billingCountry, $enabledCountries, true)) {
                            return $enabledCountries[0];
                        }
                    }

                    foreach ($paymentMethodsInfo->getCountries() as $country) {
                        if ($country->getCode() === $billingCountry) {
                            return $billingCountry;
                        }
                    }

                    return 'other';
                };

                $pane_form['paysera_checkout']['payment_country'] = [
                    '#id' => 'paysera_payment_country',
                    '#name' => 'paysera_payment_country',
                    '#type' => 'select',
                    '#options' => $this->payseraHelper->getEnabledCountries($this->pluginSettings),
                    '#default_value' => $activeCountry(),
                    '#attributes' => [
                        'style' => 'width:70%;'
                    ],
                ];

                $pane_form['paysera_checkout']['payment_method'] = [
                    '#type' => 'hidden',
                    '#attributes' => [
                        'id' => 'paysera_payment_method',
                    ]
                ];

                if ($this->pluginSettings->isGridEnabled()) {
                    $pane_form[] = $this->generateGridView($paymentMethodsInfo, $activeCountry());
                } else {
                    $pane_form[] = $this->generateListView($paymentMethodsInfo, $activeCountry());
                }
            }

            if ($this->pluginSettings->isBuyerConsentEnabled()) {
                $buyerConsent = '</br>';

                $buyerConsent .= sprintf(
                    t('Please be informed that the account information and payment initiation services will be provided to you by Paysera in accordance with these %s. By proceeding with this payment, you agree to receive this service and the service terms and conditions.'),
                    '<a href="' . t('https://www.paysera.lt/v2/en-LT/legal/pis-rules-2020') . '"> '
                    . t('rules')  .'</a>'
                );

                $pane_form['paysera_buyer_consent'] = [
                    '#markup' => $buyerConsent,
                ];
            }
        }

        return $pane_form;
    }

    public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form)
    {
        parent::submitPaneForm($pane_form, $form_state, $complete_form);

        $payseraOrderFields = $form_state->getValue('paysera_order_fields');

        $paymentCountry = isset($payseraOrderFields['paysera_checkout']['payment_country']) ?
            $payseraOrderFields['paysera_checkout']['payment_country'] :
            null
        ;
        $paymentMethod = isset($payseraOrderFields['paysera_checkout']['payment_method']) ?
            $payseraOrderFields['paysera_checkout']['payment_method'] :
            null
        ;

        try {
            $this->order->setData('paysera_payment_country', $paymentCountry)->save();
            $this->order->setData('paysera_payment_method', $paymentMethod)->save();
        } catch (EntityStorageException $exception) {
            $this->payseraHelper->getPayseraLogger()->warning(
                'Submit order data exception: ' . $exception->getMessage()
            );
        }
    }

    /**
     * @param WebToPay_PaymentMethodList $paymentMethodsInfo
     * @param string $activeCountry
     * @return array
     */
    private function generateListView(WebToPay_PaymentMethodList $paymentMethodsInfo, $activeCountry)
    {
        $form = [];

        foreach ($paymentMethodsInfo->getCountries() as $country) {
            $fieldClasses = 'paysera-methods ' . ($activeCountry === $country->getCode() ? '' : 'hide-payment-country');

            $form['paysera_checkout'][$country->getCode()] = [
                '#id' => 'paysera_payment_country_' . $country->getCode(),
                '#type' => 'fieldset',
                '#title' => $country->getTitle(),
                '#attributes' => [
                    'class' => $fieldClasses,
                ],
            ];

            foreach ($country->getGroups() as $group) {
                $form['paysera_checkout'][$country->getCode()][$group->getKey()] = [
                    '#id' => 'paysera-payment',
                    '#type' => 'markup',
                    '#markup' => $group->getTitle(),
                ];

                foreach ($group->getPaymentMethods() as $paymentMethod) {
                    $form['paysera_checkout'][$country->getCode()][$group->getKey()][$paymentMethod->getKey()][] = [
                        '#type' => 'radio',
                        '#name' => 'paysera_payment_radio',
                        '#title' =>  $paymentMethod->getTitle() . ' <img src="' . $paymentMethod->getLogoUrl() . '" />',
                        '#attributes' => [
                            'value' => $paymentMethod->getKey(),
                        ]
                    ];
                }
            }
        }

        return $form;
    }

    /**
     * @param WebToPay_PaymentMethodList $paymentMethodsInfo
     * @param string $activeCountry
     * @return array
     */
    private function generateGridView(WebToPay_PaymentMethodList $paymentMethodsInfo, $activeCountry)
    {
        $form = [];

        foreach ($paymentMethodsInfo->getCountries() as $country) {
            $fieldClasses = 'paysera-methods ' . ($activeCountry === $country->getCode() ? '' : 'hide-payment-country');

            $form['paysera_checkout'][$country->getCode()] = [
                '#id' => 'paysera_payment_country_' . $country->getCode(),
                '#type' => 'fieldset',
                '#title' => $country->getTitle(),
                '#attributes' => [
                    'class' => $fieldClasses,
                ],
            ];

            foreach ($country->getGroups() as $group) {
                $form['paysera_checkout'][$country->getCode()][$group->getKey()] = [
                    '#type' => 'table',
                    '#caption' => $group->getTitle(),
                ];

                $methods = $group->getPaymentMethods();

                $methodsRowsArray = [];
                $methodsCount = count($methods);
                $loopCount = 0;

                while ($loopCount < $methodsCount) {
                    $methodsRowsArray[] = array_slice($methods, $loopCount, self::GRID_VIEW_ROWS_NUMBER);

                    $loopCount += self::GRID_VIEW_ROWS_NUMBER;
                }

                foreach ($methodsRowsArray as $rowId => $threeMethodsArray) {
                    foreach ($threeMethodsArray as $paymentId => $payment) {
                        /**  @var $payment WebToPay_PaymentMethod */
                        $form['paysera_checkout'][$country->getCode()][$group->getKey()][$rowId]
                        ['payment_row_'.$paymentId] = [
                            '#type' => 'radio',
                            '#name' => 'paysera_payment_radio',
                            '#title' =>  ' <img src="' . $payment->getLogoUrl() . '" />',
                            '#attributes' => [
                                'value' => $payment->getKey(),
                            ],
                        ];
                    }
                }
            }
        }

        return $form;
    }
}
