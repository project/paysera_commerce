<?php
namespace Drupal\commerce_paysera\Plugin\views\display_extender;

use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Drupal\commerce_paysera\Service\PayseraSettingsHelper;
use Drupal\commerce_paysera\Entity\PluginSettings;
use Drupal\views\Annotation\ViewsDisplayExtender;
use Drupal\Core\Annotation\Translation;

/**
 * @ViewsDisplayExtender(
 *   id = "paysera_view",
 *   title = @Translation("head metadata display extender"),
 *   help = @Translation("Settings to add metatag in document head for this view."),
 *   no_ui = FALSE
 * )
 */
class PayseraView extends DisplayExtenderPluginBase
{
    /**
     * @return PluginSettings
     */
    public function getPluginSettings()
    {
        return PayseraSettingsHelper::getPluginSettings();
    }
}
