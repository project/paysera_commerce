<?php
namespace Drupal\commerce_paysera\Service;

use Drupal\commerce_paysera\Entity\PluginSettings;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_store\StoreStorageInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_log\LogStorageInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityInterface;
use Drupal\commerce_price\Price;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use WebToPayException;
use Exception;
use WebToPay;
use Drupal;

class PayseraHelper
{
    const PAYSERA_COMMERCE_MACHINE_PLUGIN_NAME = 'paysera_commerce_payment_gateway';
    const PAYSERA_SETTINGS_NAME = 'commerce_paysera.settings';
    const PAYSERA_COMMERCE_NAME = 'commerce_paysera';

    const COMMERCE_MODE_TEST = 'test';
    const COMMERCE_MODE_PROD = 'live';

    const PAYSERA_COMMERCE_TRANSLATION_DIR = 'translations';
    const PAYSERA_COMMERCE_TEMPLATES_DIR = 'templates';

    /**
     * @return LoggerInterface
     */
    public function getPayseraLogger()
    {
        return Drupal::logger(self::PAYSERA_COMMERCE_NAME);
    }

    /**
     * @param $projectId
     * @return array
     */
    public function getProjectCountries($projectId)
    {
        try {
            $paymentMethods = WebToPay::getPaymentMethodList($projectId, $this->getDefaultSiteCurrency())
                ->setDefaultLanguage($this->getCurrentLanguage())
                ->getCountries()
            ;
        } catch (WebToPayException $exception) {
            $this->getPayseraLogger()->warning('Get project countries exception: ' . $exception->getMessage());

            return [];
        }

        $availableCountries = [];

        foreach ($paymentMethods as $country) {
            $availableCountries[$country->getCode()] = $country->getTitle();
        }

        return $availableCountries;
    }

    /**
     * @param PluginSettings $pluginSettings
     * @return array
     */
    public function getEnabledCountries(PluginSettings $pluginSettings)
    {
        $enabledCountries = $pluginSettings->getEnabledCountries();

        if (!empty($enabledCountries)) {
            $projectCountries = $this->getProjectCountries($pluginSettings->getProjectId());
            $translatedEnabledCountries = [];

            if (!empty($projectCountries)) {
                foreach ($projectCountries as $countryCode => $countryTitle) {
                    if (in_array($countryCode, $enabledCountries, true)) {
                        $translatedEnabledCountries[$countryCode] = $countryTitle;
                    }
                }
            }

            return $translatedEnabledCountries;
        }

        return $this->getProjectCountries($pluginSettings->getProjectId());
    }

    /**
     * @return string
     */
    public function getDefaultSiteCurrency()
    {
        $commerceStorage = $this->getDefaultCommerceStorage();

        if ($commerceStorage !== null) {
            return $commerceStorage->getDefaultCurrencyCode();
        }

        return 'EUR';
    }

    /**
     * @param string $drupalPluginMode
     * @return string
     */
    public function commerceModeToPaysera($drupalPluginMode)
    {
        if ($drupalPluginMode === self::COMMERCE_MODE_PROD) {
            return '0';
        } elseif ($drupalPluginMode === self::COMMERCE_MODE_TEST) {
            return '1';
        }

        throw new InvalidArgumentException('Unknown drupal plugin mode given: ' . $drupalPluginMode);
    }

    /**
     * @return string
     */
    public function getCurrentLanguage()
    {
        return Drupal::languageManager()->getCurrentLanguage()->getId();
    }

    /**
     * @param $drupalLanguage
     * @return string
     */
    public function drupalLanguageToPaysera($drupalLanguage)
    {
        switch ($drupalLanguage) {
            case 'en': {
                return 'ENG';
            }
            case 'lt': {
                return 'LIT';
            }
            case 'lv': {
                return 'LAV';
            }
            case 'es': {
                return 'EST';
            }
            case 'ru': {
                return 'RUS';
            }
            case 'de': {
                return 'GER';
            }
            case 'pl': {
                return 'POL';
            }
            default: return '';
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getRequestArray(Request $request)
    {
        return array_merge($request->query->all(), $request->request->all());
    }

    /**
     * @param OrderInterface $order
     * @param string $comment
     * @return bool
     */
    public function setOrderLog(OrderInterface $order, $comment)
    {
        try {
            /** @var LogStorageInterface $logStorage */
            $logStorage = Drupal::entityTypeManager()->getStorage('commerce_log');
            $logStorage->generate($order, 'order_comment', ['comment' => $comment])->save();

            return true;
        } catch (Exception $exception) {
            $this->getPayseraLogger()->warning('Order logger exception: ' . $exception->getMessage());

            return false;
        }
    }

    /**
     * @param mixed $amount
     * @return string
     */
    public function convertOrderAmountToString($amount)
    {
        return (string) (round($amount, 2) * 100);
    }

    /**
     * @return array
     */
    public function getAvailablePaymentStatuses()
    {
        return [
            'new' => t('New'),
            'authorization' => t('Authorization'),
            'completed' => t('Completed'),
        ];
    }

    /**
     * @param array $response
     * @return Price
     */
    public function formatPriceFromPayseraResponse(array $response)
    {
        $checkConvert = array_key_exists('payamount', $response);

        $paymentAmount = ($checkConvert ? $response['payamount'] : $response['amount']) / 100;
        $paymentCurrency = $checkConvert ? $response['paycurrency'] : $response['currency'];

        return (new Price($paymentAmount, $paymentCurrency));
    }

    /**
     * @param string $orderId
     * @param array $response
     * @param string $paymentState
     * @return int|null
     */
    public function createPayment($orderId, $response, $paymentState)
    {
        try {
            $paymentStorage = Drupal::entityTypeManager()->getStorage('commerce_payment');

            $paymentParameters = [
                'payment_gateway' => PayseraHelper::PAYSERA_COMMERCE_MACHINE_PLUGIN_NAME,
                'payment_gateway_mode' => $response['test'] === '1' ?
                    self::COMMERCE_MODE_TEST
                    : self::COMMERCE_MODE_PROD
                ,
                'payment_method' => $response['payment'],
                'order_id' => $orderId,
                'remote_state' => isset($response['status']) ? $response['status'] : null,
                'amount' => $this->formatPriceFromPayseraResponse($response),
                'state' => $paymentState,
                'remote_id' => isset($response['requestid']) ? $response['requestid'] : null,
            ];

            $stateParameter = $this->getStateParameter($paymentState);

            if (!empty($stateParameter)) {
                $key = key($stateParameter);
                $paymentParameters[$key] = $stateParameter[$key];
            }

            return $paymentStorage->create($paymentParameters)->save();
        } catch (Exception $exception) {
            $this->getPayseraLogger()->warning('Create payment exception: ' . $exception->getMessage());

            return null;
        }
    }

    /**
     * @param string $orderId
     * @param string $paymentState
     * @return bool
     */
    public function isPaymentCreated($orderId, $paymentState)
    {
        try {
            $paymentStorage = Drupal::entityTypeManager()->getStorage('commerce_payment');

            /** @var EntityInterface[] $payments */
            $payments = $paymentStorage->loadByProperties([
                'order_id' => $orderId,
                'state' => $paymentState,
            ]);

            return !empty($payments);
        } catch (Exception $exception) {
            $this->getPayseraLogger()->warning('Need payment creation exception: ' . $exception->getMessage());

            return true;
        }
    }

    /**
     * @param string $orderId
     * @return string
     */
    public function getViewOrderUrl($orderId)
    {
        $userId = Drupal::currentUser()->id();

        return $userId !== 0 ? ('/user/' . $userId . '/orders/' . $orderId) : '';
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return Drupal::request()->getSchemeAndHttpHost() . '/';
    }

    /**
     * @return StoreInterface|null
     */
    private function getDefaultCommerceStorage()
    {
        try {
            /** @var $storeStorage StoreStorageInterface */
            $storeStorage = Drupal::entityTypeManager()->getStorage('commerce_store');

            return $storeStorage->loadDefault();
        } catch (Exception $exception) {
            $this->getPayseraLogger()->warning('Commerce storage exception: ' . $exception->getMessage());

            return null;
        }
    }

    /**
     * @param $status
     * @return array
     */
    private function getStateParameter($status)
    {
        if (in_array($status, ['authorization', 'completed'], true)) {
            return [$status => time()];
        }

        return [];
    }
}
