<?php
namespace Drupal\commerce_paysera\Entity;

class PluginSettings
{
    /**
     * @var int
     */
    private $projectId;

    /**
     * @var string
     */
    private $projectSign;

    /**
     * @var string
     */
    private $checkoutDescription;

    /**
     * @var bool
     */
    private $paymentListEnabled;

    /**
     * @var array
     */
    private $enabledCountries;

    /**
     * @var bool
     */
    private $gridEnabled;

    /**
     * @var bool
     */
    private $buyerConsentEnabled;

    /**
     * @var string
     */
    private $paymentStatusNew;

    /**
     * @var string
     */
    private $paymentStatusConfirmed;

    /**
     * @var string
     */
    private $paymentStatusPending;

    /**
     * @var bool
     */
    private $qualitySignEnabled;

    /**
     * @var bool
     */
    private $ownershipCodeEnabled;

    /**
     * @var string
     */
    private $ownershipCode;

    public function __construct()
    {
        $this->enabledCountries = [];
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param int $projectId
     * @return $this
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * @return string
     */
    public function getProjectSign()
    {
        return $this->projectSign;
    }

    /**
     * @param string $projectSign
     * @return $this
     */
    public function setProjectSign($projectSign)
    {
        $this->projectSign = $projectSign;

        return $this;
    }

    /**
     * @return string
     */
    public function getCheckoutDescription()
    {
        return $this->checkoutDescription;
    }

    /**
     * @param string $checkoutDescription
     * @return $this
     */
    public function setCheckoutDescription($checkoutDescription)
    {
        $this->checkoutDescription = $checkoutDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function isPaymentListEnabled()
    {
        return $this->paymentListEnabled;
    }

    /**
     * @param bool $paymentListEnabled
     * @return $this
     */
    public function setPaymentListEnabled($paymentListEnabled)
    {
        $this->paymentListEnabled = $paymentListEnabled;

        return $this;
    }

    /**
     * @return array
     */
    public function getEnabledCountries()
    {
        return $this->enabledCountries;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function addEnabledCountry($country)
    {
        $this->enabledCountries[] = $country;

        return $this;
    }

    /**
     * @param array $enabledCountries
     * @return $this
     */
    public function setEnabledCountries(array $enabledCountries)
    {
        $this->enabledCountries = $enabledCountries;

        return $this;
    }

    /**
     * @return bool
     */
    public function isGridEnabled()
    {
        return $this->gridEnabled;
    }

    /**
     * @param bool $gridEnabled
     * @return $this
     */
    public function setGridEnabled($gridEnabled)
    {
        $this->gridEnabled = $gridEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBuyerConsentEnabled()
    {
        return $this->buyerConsentEnabled;
    }

    /**
     * @param bool $buyerConsentEnabled
     * @return $this
     */
    public function setBuyerConsentEnabled($buyerConsentEnabled)
    {
        $this->buyerConsentEnabled = $buyerConsentEnabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusNew()
    {
        return $this->paymentStatusNew;
    }

    /**
     * @param string $paymentStatusNew
     * @return $this
     */
    public function setPaymentStatusNew($paymentStatusNew)
    {
        $this->paymentStatusNew = $paymentStatusNew;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusConfirmed()
    {
        return $this->paymentStatusConfirmed;
    }

    /**
     * @param string $paymentStatusConfirmed
     * @return $this
     */
    public function setPaymentStatusConfirmed($paymentStatusConfirmed)
    {
        $this->paymentStatusConfirmed = $paymentStatusConfirmed;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentStatusPending()
    {
        return $this->paymentStatusPending;
    }

    /**
     * @param string $paymentStatusPending
     * @return $this
     */
    public function setPaymentStatusPending($paymentStatusPending)
    {
        $this->paymentStatusPending = $paymentStatusPending;

        return $this;
    }

    /**
     * @return bool
     */
    public function isQualitySignEnabled()
    {
        return $this->qualitySignEnabled;
    }

    /**
     * @param bool $qualitySignEnabled
     * @return $this
     */
    public function setQualitySignEnabled($qualitySignEnabled)
    {
        $this->qualitySignEnabled = $qualitySignEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOwnershipCodeEnabled()
    {
        return $this->ownershipCodeEnabled;
    }

    /**
     * @param bool $ownershipCodeEnabled
     * @return $this
     */
    public function setOwnershipCodeEnabled($ownershipCodeEnabled)
    {
        $this->ownershipCodeEnabled = $ownershipCodeEnabled;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnershipCode()
    {
        return $this->ownershipCode;
    }

    /**
     * @param string $ownershipCode
     * @return $this
     */
    public function setOwnershipCode($ownershipCode)
    {
        $this->ownershipCode = $ownershipCode;
        return $this;
    }
}
