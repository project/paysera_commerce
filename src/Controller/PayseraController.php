<?php
namespace Drupal\commerce_paysera\Controller;

use Drupal\commerce_paysera\Service\PayseraSettingsHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_paysera\Service\PayseraHelper;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Entity\Order;
use Exception;
use WebToPay;
use Drupal;

class PayseraController extends ControllerBase
{
    /**
     * @var PayseraHelper
     */
    private $payseraHelper;

    public function __construct()
    {
        $this->payseraHelper = Drupal::service('commerce_paysera.paysera_helper');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function acceptUrl(Request $request)
    {
        $messenger = Drupal::messenger();
        $pluginSettings = PayseraSettingsHelper::getPluginSettings();

        try {
            $response = WebToPay::validateAndParseData(
                $this->payseraHelper->getRequestArray($request),
                $pluginSettings->getProjectId(),
                $pluginSettings->getProjectSign()
            );

            $orderId = $response['orderid'];

            /** @var OrderInterface $order */
            $order = Order::load($response['orderid']);

            $paymentState = $pluginSettings->getPaymentStatusNew();

            if (!$this->payseraHelper->isPaymentCreated($orderId, $paymentState)) {
                $this->payseraHelper->createPayment($orderId, $response, $paymentState);
            }

            $this->payseraHelper->setOrderLog($order, t('Customer came back to page'));

            $messenger->addMessage(t('Thank you for your order!'));

            return new RedirectResponse($this->payseraHelper->getViewOrderUrl($orderId));
        } catch (Exception $exception) {
            $this->payseraHelper->getPayseraLogger()->warning('acceptUrl exception: ' . $exception->getMessage());

            $messenger->addWarning(t('Error occurred with payment gateway module'));

            return new RedirectResponse('/');
        }
    }
}
